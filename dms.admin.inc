<?php
/**
 * @file
 * Administrative page callbacks for the dms module.
 */

/**
 * Implements hook_admin_settings().
 */

function dms_admin_settings_form($form_state) {
  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
  );

  $form['account']['dms_identifier'] = array(
    '#title' => t('Account Identifier'),
    '#type' => 'textfield',
    '#default_value' => variable_get('dms_identifier', ''),
    '#size' => 30,
    '#maxlength' => 32,
    '#required' => TRUE,
    '#description' => t('Your DeluxeMarketingSuite account identifier. Go to <a href="@dashboard">DeluxeMarketingSuite dashboard</a> to get it.', array('@dashboard' => 'https://getverticalresponse.com')),
  );

  return system_settings_form($form);
}

/**
 * Implements _form_validate().
 */
function dms_admin_settings_form_validate($form, &$form_state) {
}
