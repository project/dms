Module: Deluxe Marketing Suite
Author: Deluxe Corp. <http://drupal.org/user/????>


Description
===========
Adds the VertialResponse code to your website.

-- REQUIREMENTS --

* VerticalResponse account - sign up for free at https://getverticalresponse.com
* VerticalResponse account - sign up for free at https://verticalresponse.com



-- INSTALLATION --
Copy the 'deluxe-marketing-suite' module directory in to your Drupal
sites/all/modules directory as usual.


-- CONFIGURATION --

Visit the settings page at: Configuration » System » Deluxe Marketing Suite

In the settings page, enter account identifier obtained from https://getverticalresponse.com.
