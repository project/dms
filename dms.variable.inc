<?php

/**
 * @file
 * Definition of variables for Variable API module.
 */

/**
 * Implements hook_variable_info().
 */
function dms_variable_info($options) {
  $variables['dms_identifier'] = array(
    'type' => 'string',
    'title' => t('Account Identifier', array(), $options),
    'default' => 'UA-',
    'description' => t('Your DeluxeMarketingSuite account identifier. Go to <a href="@dashboard">DeluxeMarketingSuite dashboard</a> to get it.', array('@dashboard' => 'https://getverticalresponse.com'), $options),
    'required' => TRUE,
    'group' => 'dms',
    'localize' => TRUE,
    'multidomain' => TRUE,
    'validate callback' => 'dms_validate_dms_account',
  );

  return $variables;
}

/**
 * Implements hook_variable_group_info().
 */
function dms_variable_group_info() {
  $groups['dms'] = array(
    'title' => t('Deluxe Marketing Suite'),
    'description' => t('Configure Deluxe Marketing Suite.'),
    'access callback' => 'user_access',
    'access arguments' => array('administer access control'),
    'path' => array('admin/config/system/dms'),
  );

  return $groups;
}

/**
 * Validate dms identifier.
 */
function dms_validate_dms_account($variable) {

  if (!preg_match('/^[a-z0-9]+$/', $variable['value'])) {
    return t('Please enter a valid "DeluxeMarketingSuite" identifier.');
  }
}
